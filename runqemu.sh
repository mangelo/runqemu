#!/usr/bin/env bash
#-------------------------------------------------------------------
# runqemu - a qemu frontend written in bash.
# version 0003
#-------------------------------------------------------------------

function LocTemplate() {
    #
    # Sends a basic template to stdout.
    #
    # Parameters:
    #   none
    #
    # Returns:
    #   nothing
    #
    local TEMPLATE
    read -r -d '' TEMPLATE << EOFCFG
        #########################################################################
        #
        # runqemu - a bash script frontend for qemu.
        #
        #   Template of the topology file and brief help.
        #
        #------------------------------------------------------------------------
        #
        # Full documentation:
        #
        #   under construction
        #
        #------------------------------------------------------------------------
        #
        # Template syntax
        # ------------------
        #
        # setup
        #       cmd         string
        #       cmd         string
        #
        # vm
        #       name        string
        #       start       string [yes|no]
        #       vmid        int [0...65535]
        #       memory      int [1...16384]
        #       cpus        int [1..2]
        #       smbios      string
        #       nicmodel    string [virtio|e1000|vmxnet3]
        #       serial      int [1024...65535]
        #       monitor     int [1024...65535]
        #       hdabase     string [/path/to/qcowbase.qcow2]
        #       hdadisk     string [/path/to/qcowdisk.qcow2]
        #       cdrom       string [/path/to/file.iso]
        #       boot        string [c|d|e|f|g]
        #       uuid        string [uuid]
        #       display     string [none|curses|vnc:0...9999]
        #       rtcdate     string [date|%template]
        #       rtctime     string [time|%template]
        #       nice        int [0..19]
        #
        # nic
        #       type        string [none|tap|bridge|udp|mcast]
        #
        #   none
        #       no params
        #
        #   tap
        #       name        string
        #       ipaddr      string [ip/mask]
        #       cmd         string
        #       cmd         string
        #
        #   bridge
        #       name        string
        #       ipaddr      string [ip/mask]
        #       cmd         string
        #       cmd         string
        #
        #   udp
        #       lport       int [1024...65535]
        #       rport       int [1024...65535]
        #       raddr       string
        #
        #   mcast
        #       port        int [1024...65535]
        #
        #------------------------------------------------------------------------
EOFCFG

    echo "${TEMPLATE}" | sed -e 's/^ *//'
    return
    }

function ScriptConfig() {
    #
    # Configuration: global variables.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   RQ* variables.
    #
    LANG="C"                                                                # Language for external commands.
    PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"     # Path for executables.
    RQMAPFNAME="RUNQEMU"                                                    # Name of the topology file.
    RQQEMUBINA="qemu-system-x86_64"                                         # Qemu executable.
    RQMACADDROUI="52:54:00"                                                 # OUI for mac address.
    RQDISKSIZE="32G"                                                        # Size of qcow2 disk created.
    RQSUDOSU=1                                                              # Method for root access: 0: sudo. 1: su.
    return
    }

function ChkArgInt() {
    #
    # Check if an argument is integer.
    # Exit if is invalid.
    #
    # Parameters:
    #   $1: value to check.
    #   $2: minimum acceptable value.
    #   $3: maximum acceptable value.
    #   $4: line number.
    #
    # Returns:
    #   none.
    #
    if [ -z "$1" ]; then
        echo "ERROR: ${RQMAPFNAME}:${4} empty value."
        exit 1
    fi
    if ! [[ "$1" =~ ^[0-9]+$ ]] ; then
        echo "ERROR: ${RQMAPFNAME}:${4} value '${1}' is not a number."
        exit 1
    fi
    if [ $1 -lt $2 ] || [ $1 -gt $3 ]; then
        echo "ERROR: ${RQMAPFNAME}:${4} value '${1}' outside limits: ${2}..${3}."
        exit 1
    fi
    return
    }

function ChkArgStr() {
    #
    # Check if an argument is string.
    # Exit if is invalid.
    #
    # Parameters:
    #   $1: value to check.
    #   $2: 0: no spaces allowed. 1: spaces allowed.
    #   $3: maximum acceptable len.
    #   $4: line number.
    #
    # Returns:
    #   none.
    #
    if [ -z "$1" ]; then
        echo "ERROR: ${RQMAPFNAME}:${4} empty value."
        exit 1
    fi
    if [ "$2" -eq 0 ] && [[ "$1" =~ [[:space:]] ]]; then
        echo "ERROR: ${RQMAPFNAME}:${4} spaces not allowed in value '${1}'."
        exit 1
    fi
    if [ "${#1}" -gt "$3" ]; then
        echo "ERROR: ${RQMAPFNAME}:${4} value '${1}' too long, max ${3} chars."
        exit 1
    fi
    return
    }

function ChkArgOpt() {
    #
    # Check if an argument is an option.
    # Exit if is invalid.
    #
    # Parameters:
    #   $1: value to check.
    #   $2: options separated by colons.
    #   $3: line number.
    #
    # Returns:
    #   none.
    #
    if [ -z "$1" ]; then
        echo "ERROR: ${RQMAPFNAME}:${3} empty value."
        exit 1
    fi
    if ! [[ "$2" =~ ":${1}:" ]]; then
        echo "ERROR: ${RQMAPFNAME}:${3} unknown value '${1}'."
        exit 1
    fi
    return
    }

function LocReadTopo() {
    #
    # Read and parses the topology file.
    #
    #
    # Parameters:
    #   none
    #
    # Returns:
    #  RQ* variables.
    #
    local BASENIC
    local BASEVM
    local KEYNAME
    local KEYVALUE
    local LINNUM
    local NDXCNT
    local NDXLEN
    local NDXLST
    local NICNUM
    local NWLINE
    local STATUS
    local TLINE
    local VMNUM

    # Don't parse the file again.
    if [ "${#RQLABVMLIST[@]}" -ne 0 ]; then
        return
    fi

    # Check if topology file exist.
    if [ ! -f "$RQMAPFNAME" ]; then
        echo "ERROR: file $RQMAPFNAME not exist."
        echo "SOLUTION: create it."
        exit 1
    fi

    # Check for the ending newline.
    NWLINE=`tail -c 1 "$RQMAPFNAME" | wc -l`
    if [ $NWLINE -eq 0 ]; then
        echo "ERROR: file $RQMAPFNAME not ends with a newline."
        echo "SOLUTION: append a newline at the end of file."
        exit 1
    fi

    # Field indices for VMs.
    # Bash don't support enums, see https://stackoverflow.com/a/55556407
    NDXLST=("RQNDXVMNAME" "RQNDXVMSTART" "RQNDXVMIDENT" "RQNDXVMMEMORY"
            "RQNDXVMCPUS" "RQNDXVMSMBIOS" "RQNDXVMNICMODEL" "RQNDXVMSERIAL"
            "RQNDXVMMONITOR" "RQNDXVMHDABASE" "RQNDXVMHDADISK" "RQNDXVMCDROM"
            "RQNDXVMBOOT" "RQNDXVMUUID" "RQNDXVMDISPLAY" "RQNDXVMRTCDATE"
            "RQNDXVMRTCTIME" "RQNDXVMNICE")
    NDXLEN=${#NDXLST[@]}
    for ((NDXCNT=0; NDXCNT < $NDXLEN; NDXCNT++)); do
        eval "${NDXLST[NDXCNT]}=$NDXCNT"
    done

    # Field indices for NICs.
    # Bash don't support enums, see https://stackoverflow.com/a/55556407
    NDXLST=("RQNDXNICTYPE" "RQNDXNICNAME" "RQNDXNICIPADDR" "RQNDXNICCMD"
            "RQNDXNICLPORT" "RQNDXNICRPORT" "RQNDXNICRADDR" "RQNDXNICPORT")
    NDXLEN=${#NDXLST[@]}
    for ((NDXCNT=0; NDXCNT < $NDXLEN; NDXCNT++)); do
        eval "${NDXLST[NDXCNT]}=$NDXCNT"
    done

    # Empty lab topology.
    # Bash don't support multidimensional arrays, see https://stackoverflow.com/a/41641494
    RQARRAYMULT=100             # Multiplier for the array.
    RQLABVMLIST=()              # VM attributes (two dimensional array: [VmNum, AttrVal]).
    RQLABNICLIST=()             # NIC attributes (three dimensional array: [VmNum, NicNum, AttrVal]).
    RQLABSETPLIST=""            # Setup command list (string, commands separated by pipe).

    # Reads and parses the topology file.
    STATUS=0
    LINNUM=0
    VMNUM=0
    NICNUM=0
    BASENIC=0
    BASEVM=0
    while read TLINE; do
        LINNUM=$(($LINNUM + 1))

        # Clean the line.
        TLINE=`echo "$TLINE" | cut -d '#' -f 1 | sed -e 's/^[ \t]\+//' -e 's/[ \t]\+$//' -e 's/[ \t]\+/ /g'`
        if [ -z "$TLINE" ]; then
            continue
        fi

        # Split the line.
        KEYNAME=`echo "$TLINE" | cut -d ' ' -f 1`
        if [[ "$TLINE" =~ [[:space:]] ]]; then
            KEYVALUE=`echo "$TLINE" | cut -d ' ' -f 2-`
        else
            KEYVALUE=""
        fi

        # Check the keyword.
        case "$KEYNAME" in
            "setup")
                    if [ $STATUS -eq 0 ]; then
                        STATUS=1
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'setup' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "cmd")
                    if [ $STATUS -eq 1 ]; then
                        ChkArgStr "$KEYVALUE" 1 256 $LINNUM
                        RQLABSETPLIST="${RQLABSETPLIST}|${KEYVALUE}"
                    elif [ $STATUS -eq 3 ]; then
                        ChkArgStr "$KEYVALUE" 1 256 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICCMD]="${RQLABNICLIST[$BASENIC + $RQNDXNICCMD]}|${KEYVALUE}"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'cmd' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "vm")
                    if [ $STATUS -eq 0 ] || [ $STATUS -eq 1 ] || [ $STATUS -eq 2 ] || [ $STATUS -eq 3 ]; then
                        STATUS=2
                        NICNUM=0
                        VMNUM=$(($VMNUM + 1))
                        BASEVM=$(($VMNUM * $RQARRAYMULT))
                        BASENIC=$(($VMNUM * $RQARRAYMULT * $RQARRAYMULT + $NICNUM * $RQARRAYMULT))
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'vm' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "name")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 0 16 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMNAME]="$KEYVALUE"
                    elif [ $STATUS -eq 3 ]; then
                        ChkArgStr "$KEYVALUE" 0 16 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICNAME]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'name' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "start")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgOpt "$KEYVALUE" ":yes:no:" $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMSTART]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'start' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "vmid")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgInt "$KEYVALUE" 0 65535 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMIDENT]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'vmid' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "memory")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgInt "$KEYVALUE" 1 16384 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMMEMORY]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'memory' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "cpus")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgInt "$KEYVALUE" 1 2 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMCPUS]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'cpus' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "smbios")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 1 512 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMSMBIOS]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'smbios' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "nicmodel")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgOpt "$KEYVALUE" ":virtio:e1000:vmxnet3:" $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMNICMODEL]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'nicmodel' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "serial")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgInt "$KEYVALUE" 1024 65535 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMSERIAL]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'serial' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "monitor")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgInt "$KEYVALUE" 1024 65535 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMMONITOR]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'monitor' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "hdabase")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 1 256 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMHDABASE]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'hdabase' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "hdadisk")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 1 256 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'hdadisk' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "cdrom")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 1 256 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMCDROM]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'cdrom' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "boot")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgOpt "$KEYVALUE" ":c:d:e:f:g:" $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMBOOT]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'boot' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "uuid")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 0 36 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMUUID]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'uuid' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "display")
                    if [ $STATUS -eq 2 ]; then
                        if [[ "$KEYVALUE" =~ ^vnc:[0-9]{1,4}$ ]]; then
                            RQLABVMLIST[$BASEVM + $RQNDXVMDISPLAY]="$KEYVALUE"
                        else
                            ChkArgOpt "$KEYVALUE" ":none:curses:" $LINNUM
                            RQLABVMLIST[$BASEVM + $RQNDXVMDISPLAY]="$KEYVALUE"
                        fi
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'display' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "rtcdate")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 0 16 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMRTCDATE]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'rtcdate' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "rtctime")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgStr "$KEYVALUE" 0 16 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMRTCTIME]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'rtctime' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "nice")
                    if [ $STATUS -eq 2 ]; then
                        ChkArgInt "$KEYVALUE" 0 19 $LINNUM
                        RQLABVMLIST[$BASEVM + $RQNDXVMNICE]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'nice' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "nic")
                    if [ $STATUS -eq 2 ] || [ $STATUS -eq 3 ]; then
                        STATUS=3
                        NICNUM=$(($NICNUM + 1))
                        BASENIC=$(($VMNUM * $RQARRAYMULT * $RQARRAYMULT + $NICNUM * $RQARRAYMULT))
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'nic' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "type")
                    if [ $STATUS -eq 3 ]; then
                        ChkArgOpt "$KEYVALUE" ":none:tap:bridge:udp:mcast:" $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'type' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "ipaddr")
                    if [ $STATUS -eq 3 ]; then
                        ChkArgStr "$KEYVALUE" 0 18 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICIPADDR]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'ipaddr' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "lport")
                    if [ $STATUS -eq 3 ]; then
                        ChkArgInt "$KEYVALUE" 1024 65535 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICLPORT]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'lport' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "rport")
                    if [ $STATUS -eq 3 ]; then
                        ChkArgInt "$KEYVALUE" 1024 65535 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICRPORT]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'rport' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "port")
                    if [ $STATUS -eq 3 ]; then
                        ChkArgInt "$KEYVALUE" 1024 65535 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICPORT]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'port' keyword outside scope."
                        exit 1
                    fi
                    ;;
            "raddr")
                    if [ $STATUS -eq 3 ]; then
                        ChkArgStr "$KEYVALUE" 0 32 $LINNUM
                        RQLABNICLIST[$BASENIC + $RQNDXNICRADDR]="$KEYVALUE"
                    else
                        echo "ERROR: ${RQMAPFNAME}:${LINNUM} 'raddr' keyword outside scope."
                        exit 1
                    fi
                    ;;
            *)
                    echo "ERROR: ${RQMAPFNAME}:${LINNUM} invalid keyword '$KEYNAME'."
                    exit 1
                    ;;
        esac
    done < <(cat "$RQMAPFNAME")

    return
    }

function LocCheckTopo() {
    #
    # Check the integrity of the topology file.
    # Exit if any value is incorrect.
    #
    # Parameters:
    #   none
    #
    # Uses:
    #   RQ* variables.
    #
    # Returns:
    #   nothing.
    #

    #%%%
    #%%% TODO:
    #%%%    * test for duplicated values: tcp ports, udp ports, hdadisk, names, vmid, and so on.
    #%%%    * test for required parameters.
    #%%%    * test for ip address and domains in udp nics.
    #%%%
    return
    }

function ParseTopology() {
    #
    # Parses the RUNQEMU file.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   RQ* variables.
    #
    LocReadTopo
    LocCheckTopo
    return
    }

function ParseStatus() {
    #
    # Get the status of the VMs.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   RQVMSTATUS: array with the status of each VM.
    #        0: VM not running.
    #       >0: VM running: PID.
    #
    ParseTopology

    RQVMSTATUS=()
    local VMNUM=1
    local BASEVM=$(($VMNUM * $RQARRAYMULT))
    local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    local VMIDENT=`printf "RQ%04XVM" "${RQLABVMLIST[$BASEVM + $RQNDXVMIDENT]}"`
    while [ ! -z "$VMNAME" ]; do
        local VMDISK=${RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]}
        local ISRUNN=`ps -ww --no-headers -eo pid:1,args:1 | grep "$RQQEMUBINA" | grep "name ${VMNAME}#${VMIDENT}" | grep "hda ${VMDISK}" | head -n 1 | cut -d ' ' -f 1`
        if [ -z "$ISRUNN" ]; then
            ISRUNN="0"
        fi
        RQVMSTATUS[$VMNUM]=$ISRUNN
        VMNUM=$(($VMNUM + 1))
        BASEVM=$(($VMNUM * $RQARRAYMULT))
        VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
        VMIDENT=`printf "RQ%04XVM" "${RQLABVMLIST[$BASEVM + $RQNDXVMIDENT]}"`
    done
    return
    }

function CalcTapName() {
    #
    # Calculates the name of the TAP interface.
    # Print the name to the stdout.
    #
    # Naming convention:
    #   rqemutap[vmid:04x][nicnum:02x]
    #
    # Parameters:
    #   $1: VM ID.
    #   $2: NIC number.
    #
    # Returns:
    #   nothing.
    #
    printf "rqemutap%04x%02x" "$1" "$2"
    return
    }

function ExecAsRoot() {
    #
    # Executes the commands as root.
    # The commands are separated by the pipe symbol.
    # If any command fails, the script execution does not stop.
    #
    # Parameters:
    #   $1: commands to execute.
    #
    # Returns:
    #   RQRETURN: 0 ok. 1 error.
    #
    local XRAND=$((($RANDOM % 200) + 25))
    local XFILE=`mktemp "/dev/shm/.runqemurootXXXXXXXXXXXX"`
    echo "#!/usr/bin/env bash" > "$XFILE"
    echo "PATH=\"${PATH}\"" >> "$XFILE"

    local XCMD
    local XONE
    while IFS="|" read -ra XCMD; do
        for XONE in "${XCMD[@]}"; do
            if [ ! -z "$XONE" ]; then
                if [[ "$XONE" =~ ">" ]]; then
                    echo "$XONE" >> "$XFILE"
                else
                    echo "$XONE > /dev/null 2> /dev/null" >> "$XFILE"
                fi
            fi
        done
    done <<< "$1"
    echo "exit $XRAND" >> "$XFILE"

    local XRTCMD="sudo"
    local XRTUSR="$USER"
    if [ $RQSUDOSU -ne 0 ]; then
        XRTCMD="su root -c"
        XRTUSR="root"
    fi
    echo "=> To execute the commands, we need root privileges."
    echo "=> Enter the ${XRTUSR} password."
    chmod 700 "$XFILE"
    $XRTCMD "$XFILE"
    local ERROR=$?
    rm -f "$XFILE"

    if [ $ERROR -ne $XRAND ]; then
        RQRETURN=1
    else
        RQRETURN=0
    fi
    return
    }

function DoCmdVersion() {
    #
    # Shows the version of this script.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   nothing.
    #
    echo "runqemu version 003"
    echo "(c) 2019 by miguel scapolla"
    echo "License GPLv3+: GNU GPL version 3 or later"
    return
    }

function DoCmdHelp() {
    #
    # Shows the CLI help.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   nothing.
    #
    echo "runqemu - a tiny qemu manager."
    echo ""
    echo "usage:"
    echo "  runqemu command args..."
    echo ""
    echo "commands:"
    echo "  list:       shows the parsed topology."
    echo "  status:     shows the status of the VMs."
    echo "  setup:      executes the setup commands."
    echo "  start:      turns on all VMs."
    echo "  vmstart VM: turns on some VMs."
    echo "  stop:       turns off all VMs."
    echo "  vmstop VM:  turns off some VMs."
    echo "  template:   displays a template on stdout."
    echo "  topology:   creates a template file."
    echo "  version:    show version."
    echo "  help:       shows this help."
    return
    }

function DoCmdList() {
    #
    # Shows the parsed topology obtained from the topology file.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   nothing.
    #
    ParseTopology

    echo "Setup:"
    if [ -z "$RQLABSETPLIST" ]; then
        echo "  none."
    else
        local XCMD
        local XONE
        while IFS="|" read -ra XCMD; do
            for XONE in "${XCMD[@]}"; do
                if [ ! -z "$XONE" ]; then
                    echo "  ${XONE}"
                fi
            done
        done <<< "$RQLABSETPLIST"
    fi

    local VMNUM=1
    local BASEVM=$(($VMNUM * $RQARRAYMULT))
    local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    while [ ! -z "$VMNAME" ]; do
        echo ""
        echo "VM: Name: ${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]} - Start: ${RQLABVMLIST[$BASEVM + $RQNDXVMSTART]}"
        echo "    RAM: ${RQLABVMLIST[$BASEVM + $RQNDXVMMEMORY]} - CPUs: ${RQLABVMLIST[$BASEVM + $RQNDXVMCPUS]} - NICs: ${RQLABVMLIST[$BASEVM + $RQNDXVMNICMODEL]}"
        echo "    TTYS0: ${RQLABVMLIST[$BASEVM + $RQNDXVMSERIAL]} - Monitor: ${RQLABVMLIST[$BASEVM + $RQNDXVMMONITOR]} - HDA: ${RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]}"

        local NICNUM=1
        local BASENIC=$(($VMNUM * $RQARRAYMULT * $RQARRAYMULT + $NICNUM * $RQARRAYMULT))
        local NICTYPE=${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}
        while [ ! -z "$NICTYPE" ]; do
            local XNIC="    NIC#${NICNUM}: Type: ${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}"
            if [ "$NICTYPE" == "tap" ]; then
                XNIC="${XNIC} ${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}"
            elif [ "$NICTYPE" == "bridge" ]; then
                XNIC="${XNIC} ${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}"
            elif [ "$NICTYPE" == "udp" ]; then
                XNIC="${XNIC} ${RQLABNICLIST[$BASENIC + $RQNDXNICLPORT]} ${RQLABNICLIST[$BASENIC + $RQNDXNICRADDR]}:${RQLABNICLIST[$BASENIC + $RQNDXNICRPORT]}"
            elif [ "$NICTYPE" == "mcast" ]; then
                XNIC="${XNIC} ${RQLABNICLIST[$BASENIC + $RQNDXNICPORT]}"
            fi
            echo "$XNIC"
            NICNUM=$(($NICNUM + 1))
            BASENIC=$(($VMNUM * $RQARRAYMULT * $RQARRAYMULT + $NICNUM * $RQARRAYMULT))
            NICTYPE=${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}
        done
        VMNUM=$(($VMNUM + 1))
        BASEVM=$(($VMNUM * $RQARRAYMULT))
        VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    done
    return
    }

function DoCmdStatus() {
    #
    # Shows the status of the VMs.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   nothing.
    #
    ParseStatus
    local VMNUM=1
    local BASEVM=$(($VMNUM * $RQARRAYMULT))
    local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    while [ ! -z "$VMNAME" ]; do
        local ISRUNN=${RQVMSTATUS[$VMNUM]}
        if [ $ISRUNN -ne 0 ]; then
            ISRUNN="running pid $ISRUNN"
        else
            ISRUNN="stopped"
        fi
        echo "VM: $VMNAME - Stat: $ISRUNN"
        VMNUM=$(($VMNUM + 1))
        BASEVM=$(($VMNUM * $RQARRAYMULT))
        VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    done
    return
    }

function DoCmdSetup() {
    #
    # As root, executes the setup commands.
    #
    # Parameters:
    #   none
    #
    # Returns:
    #   nothing.
    #
    ParseTopology

    if [ -z "$RQLABSETPLIST" ]; then
        echo "no setup commands to execute."
    else
        ExecAsRoot "$RQLABSETPLIST"
        if [ $RQRETURN -eq 0 ]; then
            echo "setup executed ok."
        else
            echo "error executing setup commands."
        fi
    fi
    return
    }

function DoCmdStart() {
    #
    # Turn on all virtual machines.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   nothing.
    #
    ParseTopology

    local VMLIST=""
    local VMNUM=1
    local BASEVM=$(($VMNUM * $RQARRAYMULT))
    local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    while [ ! -z "$VMNAME" ]; do
        VMLIST="${VMLIST} ${VMNAME}"
        VMNUM=$(($VMNUM + 1))
        BASEVM=$(($VMNUM * $RQARRAYMULT))
        VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    done

    DoCmdVmStart "SRT" $VMLIST
    return
    }

function DoCmdVmStart() {
    #
    # Turn on some virtual machines.
    #
    # Parameters:
    #   $1: "VMS": called from CLI command vmstart.
    #       "SRT": called from CLI command start.
    #   $2..$N: VM names to start.
    #
    # Returns:
    #   nothing.
    #
    ParseStatus

    local VMSTART
    local STARTNUM=0
    local STARTNAMES=()
    local STARTCOMMS=()
    local STARTSETUP=""
    local WHOCALL="$1"
    shift
    local VMLIST=`echo "$@" | tr ' ' '\n' | sort | uniq | tr '\n' ' '`
    if [ `echo "$VMLIST" | wc -w` -eq 0 ]; then
        echo "no VMs to start."
        return
    fi

    for VMSTART in $VMLIST; do
        STARTNUM=$(($STARTNUM + 1))
        STARTNAMES[$STARTNUM]="$VMSTART"
        STARTCOMMS[$STARTNUM]="UNKNOWN"

        local VMNUM=1
        local BASEVM=$(($VMNUM * $RQARRAYMULT))
        local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
        while [ ! -z "$VMNAME" ]; do
            if [ "$VMNAME" == "$VMSTART" ]; then
                if [ ${RQVMSTATUS[$VMNUM]} -ne 0 ]; then
                    STARTCOMMS[$STARTNUM]="RUNNING"
                elif [ "${RQLABVMLIST[$BASEVM + $RQNDXVMSTART]}" == "no" ] && [ "$WHOCALL" == "SRT" ]; then
                    STARTCOMMS[$STARTNUM]="NOSTART"
                else
                    local XNICE=""
                    if [ ! -z ${RQLABVMLIST[$BASEVM + $RQNDXVMNICE]} ]; then
                        if [ ${RQLABVMLIST[$BASEVM + $RQNDXVMNICE]} -ne 0 ]; then
                            XNICE="nice -n ${RQLABVMLIST[$BASEVM + $RQNDXVMNICE]}"
                        fi
                    fi

                    local XDISPLAY="${RQLABVMLIST[$BASEVM + $RQNDXVMDISPLAY]}"
                    if [ -z "$XDISPLAY" ]; then
                        XDISPLAY="none"
                    fi
                    if [[ "$XDISPLAY" =~ ^vnc: ]]; then
                        XDISPLAY=`echo "$XDISPLAY" | sed -e 's/:/=:/'`
                        XDISPLAY="${XDISPLAY},lossy -vga std -device usb-ehci -device usb-tablet"
                    fi

                    local XDAEMON="-daemonize"
                    if [ "$XDISPLAY" == "curses" ]; then
                        XDAEMON=""
                    fi

                    local XUUID=""
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMUUID]}" ]; then
                        XUUID="-uuid ${RQLABVMLIST[$BASEVM + $RQNDXVMUUID]}"
                    fi

                    local XRTCDATE
                    if [[ "${RQLABVMLIST[$BASEVM + $RQNDXVMRTCDATE]}" =~ "%" ]]; then
                        XRTCDATE=`date "+${RQLABVMLIST[$BASEVM + $RQNDXVMRTCDATE]}"`
                    elif [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMRTCDATE]}" ]; then
                        XRTCDATE="${RQLABVMLIST[$BASEVM + $RQNDXVMRTCDATE]}"
                    else
                        XRTCDATE=`date "+%Y-%m-%d"`
                    fi

                    local XRTCTIME
                    if [[ "${RQLABVMLIST[$BASEVM + $RQNDXVMRTCTIME]}" =~ "%" ]]; then
                        XRTCTIME=`date "+${RQLABVMLIST[$BASEVM + $RQNDXVMRTCTIME]}"`
                    elif [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMRTCTIME]}" ]; then
                        XRTCTIME="${RQLABVMLIST[$BASEVM + $RQNDXVMRTCTIME]}"
                    else
                        XRTCTIME=`date "+%H:%M:%S"`
                    fi

                    local XRTCALL=""
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMRTCDATE]}" ] || [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMRTCTIME]}" ]; then
                        XRTCALL="-rtc base=${XRTCDATE}T${XRTCTIME},clock=vm"
                    fi

                    local XSERIAL=""
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMSERIAL]}" ]; then
                        XSERIAL="-serial telnet::${RQLABVMLIST[$BASEVM + $RQNDXVMSERIAL]},server,nowait"
                    fi

                    local XMONIT=""
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMMONITOR]}" ]; then
                        XMONIT="-monitor tcp:127.0.0.1:${RQLABVMLIST[$BASEVM + $RQNDXVMMONITOR]},server,nowait"
                    fi

                    local XSMBIOS=""
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMSMBIOS]}" ]; then
                        XSMBIOS="-smbios type=1,product='${RQLABVMLIST[$BASEVM + $RQNDXVMSMBIOS]}'"
                    fi

                    local XNICMODEL="${RQLABVMLIST[$BASEVM + $RQNDXVMNICMODEL]}"
                    if [ "$XNICMODEL" == "virtio" ]; then
                        XNICMODEL="virtio-net-pci"
                    fi

                    local XCDROM=""
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMCDROM]}" ]; then
                        XCDROM="-cdrom '${RQLABVMLIST[$BASEVM + $RQNDXVMCDROM]}'"
                    fi

                    local XBOOT="c"
                    if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMBOOT]}" ]; then
                        XBOOT="${RQLABVMLIST[$BASEVM + $RQNDXVMBOOT]}"
                    fi

                    local XNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
                    local XIDENT=`printf "RQ%04XVM" "${RQLABVMLIST[$BASEVM + $RQNDXVMIDENT]}"`

                    if [ ! -f "${RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]}" ]; then
                        if [ ! -z "${RQLABVMLIST[$BASEVM + $RQNDXVMHDABASE]}" ]; then
                            qemu-img create -q -f qcow2 -b "${RQLABVMLIST[$BASEVM + $RQNDXVMHDABASE]}" "${RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]}" > /dev/null 2> /dev/null
                        else
                            qemu-img create -q -f qcow2 "${RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]}" "$RQDISKSIZE" > /dev/null 2> /dev/null
                        fi
                    fi

                    local CMDLINE=""
                    CMDLINE="$CMDLINE $XNICE"
                    CMDLINE="$CMDLINE $RQQEMUBINA"
                    CMDLINE="$CMDLINE -name ${XNAME}#${XIDENT}"
                    CMDLINE="$CMDLINE -m ${RQLABVMLIST[$BASEVM + $RQNDXVMMEMORY]}"
                    CMDLINE="$CMDLINE -k en-us"
                    CMDLINE="$CMDLINE -cpu host"
                    CMDLINE="$CMDLINE -smp cpus=${RQLABVMLIST[$BASEVM + $RQNDXVMCPUS]}"
                    CMDLINE="$CMDLINE -enable-kvm"
                    CMDLINE="$CMDLINE -hda '${RQLABVMLIST[$BASEVM + $RQNDXVMHDADISK]}'"
                    CMDLINE="$CMDLINE $XCDROM"
                    CMDLINE="$CMDLINE -boot $XBOOT"
                    CMDLINE="$CMDLINE $XSERIAL"
                    CMDLINE="$CMDLINE $XMONIT"
                    CMDLINE="$CMDLINE $XRTCALL"
                    CMDLINE="$CMDLINE $XSMBIOS"
                    CMDLINE="$CMDLINE -display $XDISPLAY"
                    CMDLINE="$CMDLINE $XDAEMON"
                    CMDLINE="$CMDLINE $XUUID"
                    CMDLINE="$CMDLINE -net none"

                    local NICNUM=1
                    local BASENIC=$(($VMNUM * $RQARRAYMULT * $RQARRAYMULT + $NICNUM * $RQARRAYMULT))
                    local NICTYPE=${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}
                    while [ ! -z "$NICTYPE" ]; do
                        local XNICCMD
                        if [ "${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}" == "tap" ]; then
                            XNICCMD="-netdev tap,id=net${NICNUM},ifname=${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]},script=no,downscript=no"
                            local XEXIST=`ifconfig "${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}" 2> /dev/null`
                            if [ -z "$XEXIST" ]; then
                                STARTSETUP="${STARTSETUP}|tunctl -u '$USER' -t '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}'"
                                STARTSETUP="${STARTSETUP}|ifconfig '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}' up"
                                if [ ! -z "${RQLABNICLIST[$BASENIC + $RQNDXNICIPADDR]}" ]; then
                                    STARTSETUP="${STARTSETUP}|ifconfig '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}' '${RQLABNICLIST[$BASENIC + $RQNDXNICIPADDR]}' up"
                                fi
                                if [ ! -z "${RQLABNICLIST[$BASENIC + $RQNDXNICCMD]}" ]; then
                                    STARTSETUP="${STARTSETUP}|${RQLABNICLIST[$BASENIC + $RQNDXNICCMD]}"
                                fi
                            fi
                        elif [ "${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}" == "bridge" ]; then
                            local XTAPNAME=`CalcTapName "${RQLABVMLIST[$BASEVM + $RQNDXVMIDENT]}" "$NICNUM"`
                            XNICCMD="-netdev tap,id=net${NICNUM},ifname=${XTAPNAME},script=no,downscript=no"
                            local XEXIST=`ifconfig "$XTAPNAME" 2> /dev/null`
                            if [ -z "$XEXIST" ]; then
                                STARTSETUP="${STARTSETUP}|tunctl -u '$USER' -t '$XTAPNAME'"
                                STARTSETUP="${STARTSETUP}|ifconfig '$XTAPNAME' up"
                            fi
                            XEXIST=`ifconfig "${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}" 2> /dev/null`
                            if [ -z "$XEXIST" ]; then
                                STARTSETUP="${STARTSETUP}|brctl addbr '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}'"
                                STARTSETUP="${STARTSETUP}|ifconfig '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}' up"
                                if [ ! -z "${RQLABNICLIST[$BASENIC + $RQNDXNICIPADDR]}" ]; then
                                    STARTSETUP="${STARTSETUP}|ifconfig '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}' '${RQLABNICLIST[$BASENIC + $RQNDXNICIPADDR]}'"
                                fi
                                if [ ! -z "${RQLABNICLIST[$BASENIC + $RQNDXNICCMD]}" ]; then
                                    STARTSETUP="${STARTSETUP}|${RQLABNICLIST[$BASENIC + $RQNDXNICCMD]}"
                                fi
                            fi
                            XEXIST=`brctl show "${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}" 2> /dev/null | grep "$XTAPNAME"`
                            if [ -z "$XEXIST" ]; then
                                STARTSETUP="${STARTSETUP}|brctl addif '${RQLABNICLIST[$BASENIC + $RQNDXNICNAME]}' '$XTAPNAME'"
                            fi
                        elif [ "${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}" == "udp" ]; then
                            XNICCMD="-netdev socket,id=net${NICNUM},udp=${RQLABNICLIST[$BASENIC + $RQNDXNICRADDR]}:${RQLABNICLIST[$BASENIC + $RQNDXNICRPORT]},localaddr=:${RQLABNICLIST[$BASENIC + $RQNDXNICLPORT]}"
                        elif [ "${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}" == "mcast" ]; then
                            XNICCMD="-netdev socket,id=net${NICNUM},mcast=230.0.0.1:${RQLABNICLIST[$BASENIC + $RQNDXNICPORT]}"
                        else
                            XNICCMD="-netdev user,id=net${NICNUM},ipv4=off,restrict=on"
                        fi
                        local XMACDD=`printf "%04x%02x" "${RQLABVMLIST[$BASEVM + $RQNDXVMIDENT]}" "$NICNUM"`
                        CMDLINE="$CMDLINE -device ${XNICMODEL},netdev=net${NICNUM},mac=${RQMACADDROUI}:${XMACDD:0:2}:${XMACDD:2:2}:${XMACDD:4:2}"
                        CMDLINE="$CMDLINE $XNICCMD"

                        NICNUM=$(($NICNUM + 1))
                        BASENIC=$(($VMNUM * $RQARRAYMULT * $RQARRAYMULT + $NICNUM * $RQARRAYMULT))
                        NICTYPE=${RQLABNICLIST[$BASENIC + $RQNDXNICTYPE]}
                    done
                    STARTCOMMS[$STARTNUM]="$CMDLINE"
                fi
            fi
            VMNUM=$(($VMNUM + 1))
            BASEVM=$(($VMNUM * $RQARRAYMULT))
            VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
        done
    done

    if [ ! -z "$STARTSETUP" ]; then
        ExecAsRoot "$STARTSETUP"
        if [ $RQRETURN -eq 0 ]; then
            echo "nic commands executed ok."
        else
            echo "error executing nic commands."
            return
        fi
    fi

    STARTNUM=1
    VMSTART="${STARTNAMES[$STARTNUM]}"
    while [ ! -z "$VMSTART" ]; do
        CMDLINE="${STARTCOMMS[$STARTNUM]}"
        if [ "$CMDLINE" == "UNKNOWN" ]; then
            echo "unknown vm $VMSTART"
        elif [ "$CMDLINE" == "RUNNING" ]; then
            echo "running vm $VMSTART"
        elif [ "$CMDLINE" == "NOSTART" ]; then
            echo "nostart vm $VMSTART"
        else
            echo "starting vm $VMSTART"
            eval "$CMDLINE"
            sleep 5
        fi
        STARTNUM=$(($STARTNUM + 1))
        VMSTART="${STARTNAMES[$STARTNUM]}"
    done
    return
    }

function DoCmdStop() {
    #
    # Turn off all virtual machines.
    #
    # Parameters:
    #   none.
    #
    # Returns:
    #   nothing.
    #
    ParseTopology

    local ZANSWER
    echo "==> WARNING: This command stop all VMs."
    echo "==> WARNING: You must respond YES to stop the VMs."
    read -p "==> WARNING: Are you sure? [YES/no]: " -t 15 ZANSWER
    if [ "${ZANSWER}" != "YES" ]; then
        echo "stopping cancelled by the user."
        return
    fi

    local VMLIST=""
    local VMNUM=1
    local BASEVM=$(($VMNUM * $RQARRAYMULT))
    local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    while [ ! -z "$VMNAME" ]; do
        VMLIST="${VMLIST} ${VMNAME}"
        VMNUM=$(($VMNUM + 1))
        BASEVM=$(($VMNUM * $RQARRAYMULT))
        VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
    done

    DoCmdVmStop $VMLIST
    return
    }

function DoCmdVmStop() {
    #
    # Turn off some virtual machines.
    #
    # Parameters:
    #   $1..$N: VM names to stop.
    #
    # Returns:
    #   nothing.
    #
    ParseStatus

    local VMLIST=`echo "$@" | tr ' ' '\n' | sort | uniq | tr '\n' ' '`
    if [ `echo "$VMLIST" | wc -w` -eq 0 ]; then
        echo "no VMs to stop."
        return
    fi

    local VMSTOP
    for VMSTOP in $VMLIST; do
        local UNKNOWN=1
        local VMNUM=1
        local BASEVM=$(($VMNUM * $RQARRAYMULT))
        local VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
        while [ ! -z "$VMNAME" ]; do
            if [ "$VMNAME" == "$VMSTOP" ]; then
                UNKNOWN=0
                if [ ${RQVMSTATUS[$VMNUM]} -eq 0 ]; then
                    echo "stopped vm $VMSTOP"
                else
                    echo "stopping vm $VMSTOP"
                    kill "${RQVMSTATUS[$VMNUM]}" > /dev/null 2> /dev/null
                    sleep 3
                fi
            fi
            VMNUM=$(($VMNUM + 1))
            BASEVM=$(($VMNUM * $RQARRAYMULT))
            VMNAME="${RQLABVMLIST[$BASEVM + $RQNDXVMNAME]}"
        done
        if [ $UNKNOWN -ne 0 ]; then
            echo "unknown vm $VMSTOP"
        fi
    done
    return
    }

function DoCmdTemplate() {
    #
    # Display a basic template on stdout.
    #
    # Parameters:
    #   none
    #
    # Returns:
    #   nothing.
    #
    LocTemplate
    return
    }

function DoCmdTopology() {
    #
    # Create a basic template in the current directory.
    #
    # Parameters:
    #   none
    #
    # Returns:
    #   nothing.
    #
    if [ ! -f "$RQMAPFNAME" ]; then
        LocTemplate > "$RQMAPFNAME"
        echo "template file $RQMAPFNAME created."
    else
        echo "ERROR: the file $RQMAPFNAME already exists."
        echo "ERROR: creation of template file cancelled."
    fi
    return
    }

function Main() {
    #
    # Main function.
    #
    # Parameters:
    #   $1: command.
    #   $2..$N: arguments.
    #
    # Returns:
    #   nothing
    #
    local COMMAND="$1"

    ScriptConfig

    if [ -z "$COMMAND" ]; then
        DoCmdHelp
        exit 1
    fi

    case "$COMMAND" in
        "-h"|"help"|"--help")
                DoCmdHelp
                ;;
        "version")
                DoCmdVersion
                ;;
        "list")
                DoCmdList
                ;;
        "status")
                DoCmdStatus
                ;;
        "setup")
                DoCmdSetup
                ;;
        "start")
                DoCmdStart
                ;;
        "vmstart")
                shift
                DoCmdVmStart "VMS" "$@"
                ;;
        "stop")
                DoCmdStop
                ;;
        "vmstop")
                shift
                DoCmdVmStop "$@"
                ;;
        "template")
                DoCmdTemplate
                ;;
        "topology")
                DoCmdTopology
                ;;
        *)
                echo "ERROR: invalid command ${COMMAND}."
                echo "ERROR: execute 'runqemu help' for help."
                ;;
    esac
    return
    }

Main "$@"
